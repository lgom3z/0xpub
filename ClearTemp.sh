#!/bin/bash
 manager="10.26.13.23"
nodo="10.26.13.24"
nodo2="10.26.13.26"

docker images --quiet --filter=dangling=true | xargs --no-run-if-empty docker rmi;
docker ps --filter status=dead --filter status=exited --filter status=created -aq | xargs -r docker rm -v;

ssh $nodo docker images --quiet --filter=dangling=true | xargs --no-run-if-empty docker rmi;
ssh $nodo docker ps --filter status=dead --filter status=exited --filter status=created -aq | xargs -r docker rm -v;


ssh $nodo2 docker images --quiet --filter=dangling=true | xargs --no-run-if-empty docker rmi;
ssh $nodo2 docker ps --filter status=dead --filter status=exited --filter status=created -aq | xargs -r docker rm -v;